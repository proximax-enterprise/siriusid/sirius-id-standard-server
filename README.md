# sirius-id-standard-server

Using for Light Verification of SiriusID. If you want to a solution to reduce time in blockchain applications, you could use server parallel.

## Getting started

Install package dependencies:

```
npm install
```
Run server:

```
node app.js
```

Notes: you can refer link below to hook HTTPS to HTTP in necessary case:
https://ngrok.com